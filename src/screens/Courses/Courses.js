import React, { useEffect } from "react";
// import styles from "./Home.module.sass";
import TooltipGlodal from "../../components/TooltipGlodal";
import Product from "../../components/Product1/index"
import { moks } from "./moks";
import axios  from "axios";

const Courses = () => {
    const [recommended, setRecommended] = React.useState([]);
    useEffect(() => {
        const getData = async () => {
            const token = localStorage.getItem("accessToken");
            console.log(token);
            const response = await axios.get("http://gagarin-api.foowe.ru:7111/v1/students-information/getCourses", { headers: { "Authorization": `Bearer ${token}` } });
            const data = response.data;
            console.log(data);
            const newData = data.map((x, index) => ({...moks[index], product: x }));
            setRecommended(newData);
        }
        getData();
    }, [])

    return (
        <>
            <div style={{ display: "grid", "gridTemplateColumns": "repeat(3, 1fr)", gap: "24px" }}>
                {moks.map((x, index) =>
                    <Product
                        item={x}
                        key={index} 
                        withoutСheckbox
                    />)}
            </div>
            <h2 style={{fontSize: "40px", lineHeight: "1.2", margin: "20px 0"}}>Рекомендованные курсы</h2>
            <div style={{ display: "grid", "gridTemplateColumns": "repeat(3, 1fr)", gap: "24px" }}>
                {recommended.map((x, index) => 
                    <Product
                        item={x}
                        key={index}
                        withoutСheckbox
                    />)}
            </div>
            <TooltipGlodal />
        </>
    );
};

export default Courses;
