import React from "react";
import TooltipGlodal from "../../components/TooltipGlodal";
import Overview from "../ProductsDashboard/Overview";
import ProductViews from "../Home/ProductViews";


const Skills = () => {
    return (
        <>
            <Overview />
            <ProductViews />
            <TooltipGlodal />
        </>
    );
};

export default Skills;
