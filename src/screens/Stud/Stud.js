import React from "react";
// import styles from "./Home.module.sass";
import TooltipGlodal from "../../components/TooltipGlodal";
import { events } from "./moks"
import Product from "../../components/Product1/index";


const Stud = () => {
    return (
        <>
            <div style={{ display: "grid", "gridTemplateColumns": "repeat(3, 1fr)", gap: "24px" }}>
                {events.map((x, index) =>
                    <Product
                        item={x}
                        key={index}
                        withoutСheckbox
                    />)}
           </div>
            
            <TooltipGlodal />
        </>
    );
};

export default Stud;
