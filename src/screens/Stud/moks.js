import png3 from "../../static/im1.png"
import png2 from "../../static/images.png"
import png1 from "../../static/img_104x80_03.png"


export const events = [
    {
        id: 0,
        category: "orientation",
        product: "Ориентация и адаптация",
        link: "ui8.net/product/product-link",
        image: png3,
        image2x: png3,
        price: 6,
        date: "Apr 3, 2021 at 3:55 PM",
        ratingValue: 4.8,
        ratingCounter: 87,
    },
    {
        id: 1,
        category: "socialization",
        product: "Социализация и вовлечение",
        link: "ui8.net/product/product-link",
        image: png2,
        image2x: png2,
        price: 5,
        date: "Apr 6, 2021 at 6:55 PM",
        ratingValue: 4.9,
        ratingCounter: 123,
    },
    {
        id: 0,
        category: "orientation",
        product: "Ориентация и адаптация",
        link: "ui8.net/product/product-link",
        image: png1,
        image2x: png1,
        price: 6,
        date: "Apr 3, 2021 at 3:55 PM",
        ratingValue: 4.8,
        ratingCounter: 87,
    },
    {
        id: 1,
        category: "socialization",
        product: "Социализация и вовлечение",
        link: "ui8.net/product/product-link",
        image: png3,
        image2x: png3,
        price: 5,
        date: "Apr 6, 2021 at 6:55 PM",
        ratingValue: 4.9,
        ratingCounter: 123,
    }, {
        id: 0,
        category: "orientation",
        product: "Ориентация и адаптация",
        link: "ui8.net/product/product-link",
        image: png2,
        image2x: png2,
        price: 6,
        date: "Apr 3, 2021 at 3:55 PM",
        ratingValue: 4.8,
        ratingCounter: 87,
    },
    {
        id: 1,
        category: "socialization",
        product: "Социализация и вовлечение",
        link: "ui8.net/product/product-link",
        image: png1,
        image2x: png1,
        price: 5,
        date: "Apr 6, 2021 at 6:55 PM",
        ratingValue: 4.9,
        ratingCounter: 123,
    },
]