import React from "react";
import styles from "./Row.module.sass";
import Cell from "./Cell";

const Row = ({ item }) => {
  return (
    <div className={styles.row}>
      <div className={styles.col}>
        <div className={styles.item}>
          <div className={styles.preview}>
            <img srcSet={`${item.image2x} 2x`} src={item.image} alt="Product" />
          </div>
          <div className={styles.details}>
            <div
              className={styles.post}
              dangerouslySetInnerHTML={{ __html: item.title }}
            ></div>
          </div>
        </div>
      </div>
      <div className={styles.col}>
        <div className={styles.label}>Views</div>
        <Cell item={item.views} greenIndicator />
      </div>
      <div className={styles.col}>
        <div className={styles.label}>Engagement</div>
        <Cell item={item.engagement} blueIndicator />
      </div>
    </div>
  );
};

export default Row;
