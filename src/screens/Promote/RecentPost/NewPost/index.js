import React from "react";
import styles from "./NewPost.module.sass";
import cn from "classnames";
import Icon from "../../../../components/Icon";
import Product from "../../../../components/Product/index";
import { category } from "../../../../mocks/qustionCategory";

const NewPost = ({ setIsOpenModal, setIsOpenModalQuestion, setCategory }) => {
  // const [completeQe, setComplete] = React.useState([]);

  
  const OnclickProduct = (category) => {
    setCategory(category);
    setIsOpenModalQuestion(true)
    setIsOpenModal(false)
  }
  return (
    <div className={styles.post}>
      <div className={cn("title-green", styles.title)}>Онбординг</div>
      <div className={styles.field}>
        <div>Перед началом использования LMS предлагаем вам пройти обучающий онбординг, который поможет вам чуть лучше узнать о всех возможностях, которые предлагает колледж</div>
      </div>
      <div className="products" style={{ marginTop:"20px", display: "grid", "gridTemplateColumns": "repeat(2, 1fr)", gap: "10px"}}>
        {category.map((x, index) => (
          <Product
            onClick={() => OnclickProduct(x.category)}
            className={styles.product}
            item={x}
            key={index}
            withoutСheckbox
          />
        ))}
      </div>
      <div className={styles.foot} style={{display: "flex", "justifyContent" : "flex-end"}}>
        <button className={cn("button", styles.button)} onClick={() => setIsOpenModal(false)}>
          <span>Пропустить</span>
          <Icon name="arrow-right" size="24" />
        </button>
      </div>
    </div>
  );
};

export default NewPost;
