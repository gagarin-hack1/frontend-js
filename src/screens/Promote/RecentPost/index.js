import React, { useState } from "react";
import styles from "./RecentPost.module.sass";
import Modal from "../../../components/Modal";
import Row from "./Row";
import NewPost from "./NewPost";

// data
import { posts } from "../../../mocks/posts";

// const intervals = ["Last 7 days", "This month", "All time"];

const RecentPost = ({ className }) => {
  // const [sorting, setSorting] = useState(intervals[0]);
  const [visibleModal, setVisibleModal] = useState(false);

  return (
    <>
        <div className={styles.table}>
          <div className={styles.row}>
            <div className={styles.col}>Предмет</div>
            <div className={styles.col}>Количество тема</div>
            <div className={styles.col}>Количество ответов</div>
          </div>
          {posts.map((x, index) => (
            <Row item={x} key={index} />
          ))}
        </div>
      <Modal
        outerClassName={styles.outer}
        visible={visibleModal}
        onClose={() => setVisibleModal(false)}
      >
        <NewPost />
      </Modal>
    </>
  );
};

export default RecentPost;
