import React from "react";
import styles from "./Home.module.sass";
import TooltipGlodal from "../../components/TooltipGlodal";
import NewPost from "../Promote/RecentPost/NewPost";
import QuestionsStart from "../../components/QuestionsStart";
import Modal from "../../components/Modal";
import { moks } from "../Courses/moks";
import Product from "../../components/Product1/index"
import CustomerList from "../CustomerList";
import Shop from "../Shop";


const Home = () => {
    const [isOpenModal, setIsOpenModal] = React.useState(true);
    const [isOpenModalQuestion, setIsOpenModalQuestion] = React.useState(false);
    const [category, setCategory] = React.useState(null);

    return (
        <>
            <div style={{ display: "grid", "gridTemplateColumns": "repeat(2, 1fr)", gap: "24px", marginBottom: "94px" }}>
                <div style={{ display: "grid", "gridTemplateColumns": "repeat(2, 1fr)",gap: "24px", padding: "24px", borderRadius: "8px" }}>
                    {moks.map((x, index) =>
                        <Product
                            item={x}
                            key={index}
                            withoutСheckbox
                        />)}
                </div>
                <CustomerList />
            </div>
            <Shop />
            <TooltipGlodal />
            <Modal
                outerClassName={styles.outer}
                visible={isOpenModal}
                onClose={() => setIsOpenModal(false)}
            >
                <NewPost setIsOpenModal={setIsOpenModal} setIsOpenModalQuestion={setIsOpenModalQuestion} setCategory={setCategory}/>
            </Modal>

            <Modal
                outerClassName={styles.outer}
                visible={isOpenModalQuestion}
                onClose={() => setIsOpenModalQuestion(false)}
            >
                <QuestionsStart category={category} setIsOpenModalQuestion={setIsOpenModalQuestion} setIsOpenModal={setIsOpenModal}/>
            </Modal>

        </>
    );
};

export default Home;
