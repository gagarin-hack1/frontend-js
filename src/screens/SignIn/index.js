import React, { useState } from "react";
import cn from "classnames";
import styles from "./SignIn.module.sass";
import { use100vh } from "react-div-100vh";
import { Link, useNavigate } from "react-router-dom";
import TextInput from "../../components/TextInput";
import Image from "../../components/Image";
import  axios  from "axios";
import { useAuth } from '../../screens/PrivateRoute/AuthContext';
const SignIn = () => {
  const heightWindow = use100vh();
  const {setUser} = useAuth();
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const handleOnClick = async () => {
    try {
      console.log(login, password);
      // Отправка запроса на сервер для аутентификации
      const response = await axios.post("http://gagarin-api.foowe.ru:7111/v1/auth/login", { login, password });
      const { access_token } = response.data;
      console.log(response);
      // Сохранение токена в localStorage
      localStorage.setItem("accessToken", access_token);
     
      console.log("GDFFDDF")
      setUser(access_token);
      setTimeout(() => navigate('/home'),100)
    } catch (error) {
      console.error("Ошибка при аутентификации:", error);
    }
    
  }

  return (
    <div className={styles.login} style={{ minHeight: heightWindow }}>
      <div className={styles.wrapper}>
        <Link className={styles.logo} to="/sign-in">
          <Image
            className={styles.pic}
            src="/images/logoIThub-light.png"
            srcDark="/images/logoIThub.png"
            alt="Core"
          />
        </Link>
        <div className={cn("h2", styles.title)}>Вход</div>
        <div className={styles.head}>
          <div className={styles.subtitle}>Пройди регистрацию с готовым аккаунтом</div>
          <div className={styles.btns}>
            <button className={cn("button-stroke", styles.button)}>
              <img src="/images/content/google.svg" alt="Google" />
              Google
            </button>
            <button className={cn("button-stroke", styles.button)}>
              <Image
                className={styles.pic}
                src="/images/content/apple-dark.svg"
                srcDark="/images/content/apple-light.svg"
                alt="Apple"
              />
              Apple ID
            </button>
          </div>
        </div>
        <div className={styles.body}>
          <div className={styles.subtitle}>Или продолжи с почтой</div>
          <TextInput
            className={styles.field}
            name="email"
            type="email"
            placeholder="Your email"
            required
            icon="mail"
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          />
          <TextInput
            className={styles.field}
            name="password"
            type="password"
            placeholder="Password"
            required
            icon="lock"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <button className={cn("button", styles.button)} onClick={handleOnClick}>Войти</button>
          <div className={styles.info}>
            Нет аккаунта?{" "}
            <span className={styles.link} onClick={() => navigate("/sign-up")}>
              Зарегистрируйся
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
