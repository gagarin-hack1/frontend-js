// PrivateRoute.js
import React from 'react';
import { Navigate } from 'react-router-dom';
import { useAuth } from './AuthContext';

export const PrivateRoute = ({ children }) => {
    const { user, isLoading } = useAuth();

    if (isLoading) {
        return null; // Возвращаем null, пока isLoading равен true
    }

    return user ? children : <Navigate to="/" />;
};


