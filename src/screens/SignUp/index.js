import React from "react";
import cn from "classnames";
import styles from "./SignUp.module.sass";
import { use100vh } from "react-div-100vh";
import { Link } from "react-router-dom";
import Image from "../../components/Image";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import TextInput from "../../components/TextInput";


const SignUp = () => {
  const heightWindow = use100vh();
  const navigate = useNavigate();
  const [login, setLogin] = React.useState("");
  const [password, setPassword] = React.useState("");

  const onContinue = async () => {
    try {
     
      // Отправка запроса на сервер для аутентификации
      const response = await axios.post("http://gagarin-api.foowe.ru:7111/v1/auth/reg",{ login, password });
      
      const { access_token } = response.data;
      // Сохранение токена в localStorage
      localStorage.setItem("accessToken", access_token);
      window.location.href = '/home'
    } catch (error) {
      console.error("Ошибка при аутентификации:", error);
    }
    
  }

  return (
    <div className={styles.login} style={{ minHeight: heightWindow }}>
      <div className={styles.wrapper}>
        <Link className={styles.logo} to="/sign-in">
          <Image
            className={styles.pic}
            src="/images/logoIThub-light.png"
            srcDark="/images/logoIThub.png"
            alt="Core"
          />
        </Link>
        <div className={cn("h2", styles.title)}>Регистрация</div>
        <div className={styles.head}>
          <div className={styles.subtitle}>Пройди регистрацию с готовым аккаунтом</div>
          <div className={styles.btns}>
            <button className={cn("button-stroke", styles.button)}>
              <img src="/images/content/google.svg" alt="Google" />
              Google
            </button>
            <button className={cn("button-stroke", styles.button)}>
              <Image
                className={styles.pic}
                src="/images/content/apple-dark.svg"
                srcDark="/images/content/apple-light.svg"
                alt="Apple"
              />
              Apple ID
            </button>
          </div>
        </div>
        <div className={styles.body}>
          <div className={styles.subtitle}>Или продолжи с почтой</div>
          <TextInput
            className={styles.field}
            name="email"
            type="email"
            placeholder="Your email"
            required
            icon="mail"
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          />
          <TextInput
            className={styles.field}
            name="password"
            type="password"
            placeholder="Password"
            required
            icon="lock"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <button className={cn("button", styles.button)} onClick={onContinue}>Продолжить</button>
          <div className={styles.info}>
            Уже есть аккаунт?{" "}
            <span className={styles.link} onClick={() => navigate("/")}>
              Войти
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
