import React from "react";
import cn from "classnames";
import styles from "./Entry.module.sass";
import TextInput from "../../../components/TextInput";
import Image from "../../../components/Image";

const Entry = ({ onConfirm, login, setLogin, password, setPassword }) => {
  return (
    <div className={styles.entry}>
      <div className={styles.head}>
        <div className={styles.info}>Войти с Open account</div>
        <div className={styles.btns}>
          <button className={cn("button-stroke", styles.button)}>
            <img src="/images/content/google.svg" alt="Google" />
            Google
          </button>
          <button className={cn("button-stroke", styles.button)}>
            <Image
              className={styles.pic}
              src="/images/content/apple-dark.svg"
              srcDark="/images/content/apple-light.svg"
              alt="Apple"
            />
            Apple ID
          </button>
        </div>
      </div>
      <div className={styles.body}>
        <div className={styles.info}>или продолжить с email</div>
        <TextInput
          className={styles.field}
          name="email"
          type="email"
          placeholder="Your email"
          required
          icon="mail"
          value={login}
          onChange={(e) => setLogin(e.target.value)}
        />
        <TextInput
          className={styles.field}
          name="password"
          type="password"
          placeholder="Password"
          required
          icon="lock"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button className={cn("button", styles.button)} onClick={onConfirm}>
          Продолжить
        </button>
      </div>
    </div>
  );
};

export default Entry;
