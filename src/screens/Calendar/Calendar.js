import React from "react";
import styles from "./Calendar.module.sass";
import TooltipGlodal from "../../components/TooltipGlodal";


const Calendar = () => {
    return (
        <>
            <div >Календарь</div>
            <div className={styles.cal}>
                <img  src ="./images/calendarview.png" alt="calendarview"/>
            </div>
            <TooltipGlodal />
        </>
    );
};

export default Calendar;
