import React from "react";
// import styles from "./Home.module.sass";
import TooltipGlodal from "../../components/TooltipGlodal";
import img from "../../static/Rectangle.png"


const OrientationMap = () => {
    return (
        <>
            <img src={img} alt="map" />
            <TooltipGlodal />
        </>
    );
};

export default OrientationMap;
