import React, { useState } from "react";
import cn from "classnames";
import styles from "./Shop.module.sass";
import Card from "../../components/Card";
import Follower from "./Follower";

// data
import { followers } from "../../mocks/followers";

const navigation = ["Заявки", "Друзья"];

const Shop = () => {
  const [activeIndex, setActiveIndex] = useState(1);

  return (
    <>
      <div className={styles.shop}> 
        <Card className={styles.card}>
          <div className={styles.control}>
            <div className={styles.nav}>
              {navigation.map((x, index) => (
                <button
                  className={cn(styles.link, {
                    [styles.active]: index === activeIndex,
                  })}
                  onClick={() => setActiveIndex(index)}
                  key={index}
                >
                  {x}
                </button>
              ))}
            </div>
          </div>
          <div className={styles.wrap}>
            {activeIndex === 1 && (
              <>
                <div className={styles.followers}>
                  {followers.map((x, index) => (
                    <Follower
                      className={styles.follower}
                      item={x}
                      key={index}
                      followers
                    />
                  ))}
                </div>
              </>
            )}
          </div>
        </Card>
      </div>
    </>
  );
};

export default Shop;
