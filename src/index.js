import React from "react";
import ReactDOM from "react-dom/client";
import { AuthProvider } from "./screens/PrivateRoute/AuthContext";
import { createBrowserRouter, RouterProvider} from "react-router-dom";
import "./styles/app.sass";
import Page from "./components/Page";
import Home from "./screens/Home";
import SignUp from "./screens/SignUp";
import SignIn from "./screens/SignIn";
import OrientationAdaptation from "./screens/OrientationAdaptation/OrientationMap";
import Forums from "./screens/Forums/Forums";
import Stud from "./screens/Stud/Stud";
import Sport from "./screens/Sport/Sport";
import Events from "./screens/Events/Events";
import Skills from "./screens/Skills/Skills";
import Courses from "./screens/Courses/Courses"
import Rules from "./screens/Rules/Rules";
import { PrivateRoute } from "./screens/PrivateRoute/PrivateRoute";
import CustomerList from "./screens/CustomerList/index";
import Shop from "./screens/Shop";
import Calendar from "./screens/Calendar/Calendar";

const routes = createBrowserRouter([
    {   
        path: '/',
        element: <SignIn />
    },
    {
        path: '/sign-up',
        element: <SignUp/>
    },
    {
        path: '/home',
        element: (
            <PrivateRoute>
                <Page title="Главная"><Home/></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/orientation/map',
        element: (
            <PrivateRoute>
               <Page title="Интерактивная карта кампуса"><OrientationAdaptation /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/orientation/forums',
        element: (
            <PrivateRoute>
               <Page title="Форумы"><Forums /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/orientation/rules',
        element: (
            <PrivateRoute>
              <Page title="Правила платформы"><Rules /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/socialization/stud',
        element: (
            <PrivateRoute>
               <Page title="Студенческие объединения"><Stud /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/socialization/sport',
        element: (
            <PrivateRoute>
               <Page title="Спорт/Киберспорт"><Sport /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/recommendation/skills',
        element: (
            <PrivateRoute>
               <Page title="Скиллы"><Skills /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/recommendation/courses',
        element: (
            <PrivateRoute>
              <Page title="Учебные курсы"><Courses /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/friends',
        element: (
            <PrivateRoute>
             <Page title="Друзья"><Shop /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/career',
        element: (
            <PrivateRoute>
              <Page title="Карьера"><CustomerList /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/recommendation/courses',
        element: (
            <PrivateRoute>
              <Page title="Курсы"><Courses /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/socialization/events',
        element: (
            <PrivateRoute>
              <Page title="Мероприятия"><Events /></Page>
            </PrivateRoute>
        ),
    },
    {
        path: '/calendar',
        element: (
            <PrivateRoute>
              <Page title="Календарь"><Calendar /></Page>
            </PrivateRoute>
        ),
    },

    
    
]);
ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <AuthProvider>
            <RouterProvider router={routes} />
        </AuthProvider>
    </React.StrictMode>
);


        // <BrowserRouter>
        //     <Routes>
        //         <Route path="home" element={<Page title="Главная"><Home/></Page>} />
        //         <Route path="orientation/map" element={<Page title="Интерактивная карта кампуса"><OrientationAdaptation /></Page>} />
        //         <Route path="orientation/forums" element={<Page title="Форумы"><Forums /></Page>} />
        //         <Route path="orientation/rules" element={<Page title="Правила платформы"><Rules /></Page>} />
        //         <Route path="socialization/stud" element={<Page title="Студенческие объединения"><Stud /></Page>} />
        //         <Route path="socialization/sport" element={<Page title="Спорт/Киберспорт"><Sport /></Page>} />
        //         <Route path="socialization/events" element={<Page title="Мероприятия"><Events /></Page>} />
        //         <Route path="recommendation/skills" element={<Page title="Мониторинг скиллов"><Skills /></Page>} />
        //         <Route path="recommendation/courses" element={<Page title="Курсы"><Courses /></Page>} />
        //         <Route path="friends" element={<Page title="Друзья"><Friends /></Page>} />
        //         <Route path="career" element={<Page title="Карьера"><Career /></Page>} />
        //         <Route path="sign-up" element={<SignUp />} />
        //         <Route index path="/" element={<SignIn />} />
        //         <Route path="pagelist" element={<PageList />} />
        //         <Route element={<Navigate to="/sign-in" />} /> 

        //     </Routes>
        // </BrowserRouter>

