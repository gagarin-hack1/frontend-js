import img1 from "../static/img_square_07.png";
import img2 from "../static/img_square_09.png";

export const customers = [
  {
    id: 1,
    title: "Frontend Developer",
    image: img1,
    salary: "₽60,000 - ₽80,000",
    dateAdded: "2024-04-10",
  },
  {
    id: 2,
    title: "Backend Developer",
    image: img2,
    salary: "₽70,000 - ₽90,000",
    dateAdded: "2024-04-09",
  },
  {
    id: 3,
    title: "Full Stack Developer",
    image: img1,
    salary: "₽80,000 - ₽100,000",
    dateAdded: "2024-04-08",
  },
  {
    id: 4,
    title: "UI/UX Designer",
    image: img2,
    salary: "₽70,000 - ₽90,000",
    dateAdded: "2024-04-07",
  },
  {
    id: 5,
    title: "Data Scientist",
    image: img1,
    salary: "₽90,000 - ₽110,000",
    dateAdded: "2024-04-06",
  },
  {
    id: 6,
    title: "DevOps Engineer",
    image: img2,
    salary: "₽80,000 - ₽100,000",
    dateAdded: "2024-04-05",
  },
];
