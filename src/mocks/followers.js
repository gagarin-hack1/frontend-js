import png3 from "../static/im1.png"
import png2 from "../static/images.png"
import png1 from "../static/img_104x80_03.png"

export const followers = [
  {
    id: 0,
    man: "Илья Брегман",
    avatar: "/images/content/avatar-1.jpg",
    profile: "Дизайнер",
    course: 4,
    gallery: [
      {
        image: png1,
        image2x: png1,
      },
      {
        image: png2,
        image2x: png2,
      },
      {
        image: png3,
        image2x: png3,
      },
    ],
  },
  {
    id: 1,
    man: "Ярослав Осокин",
    avatar: "/images/content/avatar-2.jpg",
    profile: "Team Lead",
    course: 1,
    gallery: [
      {
        image: png2,
        image2x: png2,
      },
      {
        image: png1,
        image2x: png1,
      },
      {
        image: png3,
        image2x: png3,
      },
    ],
  },
  {
    id: 2,
    man: "Попов Александр",
    avatar: "/images/content/avatar-3.jpg",
    profile: "Full Stack Developer",
    course: 3,
    message: true,
    gallery: [
      {
        image: png3,
        image2x: png3,
      },
      {
        image: png1,
        image2x: png1,
      },
      {
        image: png2,
        image2x: png2,
      },
    ],
  },
  {
    id: 3,
    man: "Артем Валеев",
    profile: "Backend Developer",
    course: 1,
    avatar: "/images/content/avatar-1.jpg",
    gallery: [
      {
        image: png2,
        image2x: png2,
      },
      {
        image: png3,
        image2x: png3,
      },
      {
        image: png1,
        image2x: png1,
      },
    ],
  },
  {
    id: 4,
    man: "Артем Плужников",
    profile: "Лютый ML Developer",
    course: 1,
    avatar: "/images/content/avatar-2.jpg",
    message: true,
    gallery: [
      {
        image: png1,
        image2x: png1,
      },
      {
        image: png3,
        image2x: png3,
      },
      {
        image: png2,
        image2x: png2,
      },
    ],
  },
];
