import png1 from "../static/asdsa.png"
import png2 from "../static/images.png"
import png3 from "../static/img_104x80_01.png"
import png4 from "../static/img_1x80_03.png"

export const category = [
    {
        id: 0,
        category: "orientation",
        product: "Ориентация и адаптация",
        link: "ui8.net/product/product-link",
        image: png3,
        image2x: png3,
        price: 6,
        date: "Apr 3, 2021 at 3:55 PM",
        ratingValue: 4.8,
        ratingCounter: 87,
    },
    {
        id: 1,
        category: "socialization",
        product: "Социализация и вовлечение",
        link: "ui8.net/product/product-link",
        image: png2,
        image2x: png2,
        price: 5,
        date: "Apr 6, 2021 at 6:55 PM",
        ratingValue: 4.9,
        ratingCounter: 123,
    },
    {
        id: 2,
        category: "study",
        product: "Учебная траектория",
        link: "ui8.net/product/product-link",
        image: png4,
        image2x: png4,
        price: 8,
        date: "Apr 10, 2021 at 10:55 PM",   
    },
    {
        id: 3,
        category: "career",
        product: "Карьерное развитие и поддержка",
        link: "ui8.net/product/product-link",
        image: png1,
        image2x: png1,
        price: 9,
        date: "Apr 12, 2021 at 12:55 PM",
        ratingValue: 4.6,
        ratingCounter: 12,
    },
];
