import React, { useState } from "react";
import cn from "classnames";
import styles from "./Product.module.sass";
import Icon from "../Icon";
import Checkbox from "../Checkbox";

const Product = ({
    className,
    item,
    value,
    onChange,
    released,
    withoutСheckbox,
}) => {
    const [visible, setVisible] = useState(false);

    const handleClick = () => {
        onChange();
        setVisible(!visible);
    };

    return (
        <div
        
            className={cn(styles.product, className, {
                [styles.active]: visible,
            })}
        >
            <div className={styles.preview}>
                {!withoutСheckbox && (
                    <Checkbox
                        className={styles.checkbox}
                        classCheckboxTick={styles.checkboxTick}
                        value={value}
                        onChange={() => handleClick()}
                    />
                )}
                <img
                    srcSet={`${item.image2x} 2x`}
                    src={item.image}
                    alt="Product"
                />
            </div>
            <div className={styles.line}>
                <div className={styles.title}>{item.product}</div>
                {item.price > 0 ? (
                    <div className={styles.price} style={{ display: "flex", alignItems: "center" }}><span>{item.price}</span><Icon name="heart" size="14" /></div>
                ) : (
                    <div className={styles.empty}>{item.price}</div>
                )}
            </div>
                <div className={styles.date}>
                    <Icon name="clock" size="24" /> {item.date}
                </div>
        </div>
    );
};

export default Product;
