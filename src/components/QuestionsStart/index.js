/* eslint-disable no-unused-vars */
import React from "react";
import styles from "./QuestionsStart.module.sass";
import cn from "classnames";
import Icon from "../../components/Icon";
import { questions } from "../../mocks/questionsstart";
import { insertText } from "draft-js/lib/DraftModifier";
import axios from "axios";

const QuestionsStart = ({ category, setIsOpenModal, setIsOpenModalQuestion }) => {
  const [pageNum, setPageNum] =  React.useState(0);
  const [answers, setAnswers] = React.useState([])
  const [answerfield, setAnswerfield] = React.useState("")

  const handleOnClick = () => {
    setPageNum(prev => prev + 1)
    setAnswers(prev => [...prev, answerfield])
    setAnswerfield("");
  }
  const handleOnClickFirst = () => {
    setPageNum(prev => prev + 1)
  }
  const backToCategory = () => {
    const items =  [...answers, answerfield ]
    setIsOpenModal(true)
    setIsOpenModalQuestion(false)
    
    const token = localStorage.getItem("accessToken")
    axios.post("http://gagarin-api.foowe.ru:7111/v1/students-information", { type: category, answers: items.join(",") }, { headers: {"Authorization" : `Bearer ${token}`} });
  }
  return (
    <div className={styles.post}>
      <div className={cn("title-green", styles.title)}>{questions[category].rus} {pageNum !== 0 ? (<span>{pageNum}/{(questions[category].qe).length}</span>):(<></>)}</div>
      <div>{pageNum===0 ? 
          (<div className={styles.preview}>
            <img src="/images/content/bg-video.jpg" alt="Video" />
          </div>) : (
          <div className={styles.qublock}>
            <span className={styles.qu}>
              {questions[category].qe[pageNum-1]}
            </span>
            
          </div> )}
      </div>
      <div className={styles.foot}>

        {pageNum !== (questions[category].qe).length && pageNum !== 0 ?
        (<div className={styles.inputfield}>
        <div className={styles.field}>
          <textarea className={styles.textarea} value={answerfield}  name="post" placeholder=" Напиши свой ответ тут" onChange={(e) => setAnswerfield(e.target.value)} />
        </div>
        <button className={cn("button", styles.button)} onClick={()=>handleOnClick()} >
          <span>Далее</span>
          <Icon name="arrow-right" size="24" />
        </button>
        </div>) : (<></>)}
        
        {pageNum === 0 ?
        (<div className={styles.inputfield}>
        <button className={cn("button", styles.button)} onClick={()=>handleOnClickFirst()} >
          <span>Далее</span>
          <Icon name="arrow-right" size="24" />
        </button>
        </div>) : (<> </>)}

        {pageNum === (questions[category].qe).length ?
        (<div className={styles.inputfield}>
        <div className={styles.field}>
          <textarea className={styles.textarea} value={answerfield} name="post" placeholder=" Напиши свой ответ тут" onChange={(e) => setAnswerfield(e.target.value)} />
        </div>
        <button className={cn("button", styles.button)} onClick={()=>backToCategory()} >
          <span>Отправить</span>
          <Icon name="arrow-right" size="24" />
        </button>
        </div>) : (<></>)}

      </div>
    </div>
  );
};

export default QuestionsStart;
