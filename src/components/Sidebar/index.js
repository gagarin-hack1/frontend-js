import React, { useState } from "react";
import styles from "./Sidebar.module.sass";
import { Link, NavLink, useLocation } from "react-router-dom";
import cn from "classnames";
import Icon from "../Icon";
import Theme from "../Theme";
import Dropdown from "./Dropdown";
import Help from "./Help";
import Image from "../Image";

const navigation = [
    {
        title: "Главная",
        icon: "home",
        url: "/home",   
    },
    {
        title: "Учеба",
        slug: "education",
        icon: "pinterest",
        dropdown: [
            {
                title: "Курсы",
                url: "/recommendation/courses",
            },
            {
                title: "Мониторинг скиллов",
                url: "/recommendation/skills",
            },
        ],
    },
    {
        title: "Студенческая жизнь",
        slug: "socialization",
        icon: "map",
        dropdown: [
            {
                title: "Студенческие объединения",
                url: "/socialization/stud",
            },
        ],
    },
    {
        title: "О колледже",
        slug: "orientation",
        icon: "diamond",    
        dropdown: [
            {
                title: "Интерактивная карта кампуса",
                url: "/orientation/map",
            },
            {
                title: "Форумы",
                url: "/orientation/forums",
            },
            {
                title: "Правила платформы",
                url: "/orientation/rules",
            },
        ],
    },
    {
        title: "Карьера",
        slug: "career",
        icon: "pinterest",
        url: "/career",
    },
    {
        title: "Друзья",
        slug: "friends",
        icon: "pinterest",
        url: "/friends",
    },
];

const Sidebar = ({ className, onClose }) => {
    const [visibleHelp, setVisibleHelp] = useState(false);
    const [visible, setVisible] = useState(false);

    const { pathname } = useLocation();

    return (
        <>
            <div
                className={cn(styles.sidebar, className, {
                    [styles.active]: visible,
                })}
            >
                <button className={styles.close} onClick={onClose}>
                    <Icon name="close" size="24" />
                </button>
                <Link className={styles.logo} to="/home" onClick={onClose}>
                    <Image
                        className={styles.pic}
                        src="/images/logoIThub-light.png"
                        srcDark="/images/logoIThub.png"
                        alt="Core"
                    />
                </Link>
                <div className={styles.menu}>
                    {navigation.map((x, index) =>
                        x.url ? (
                            <NavLink
                                className={cn(styles.item, {
                                    [styles.active]: pathname === x.url,
                                })}
                                to={x.url}
                                key={index}
                                onClick={onClose}
                            >
                                <Icon name={x.icon} size="24" />
                                {x.title}
                            </NavLink>
                        ) : (
                            <Dropdown
                                className={styles.dropdown}
                                visibleSidebar={visible}
                                setValue={setVisible}
                                key={index}
                                item={x}
                                onClose={onClose}
                            />
                        )
                    )}
                </div>
                <button
                    className={styles.toggle}
                    onClick={() => setVisible(!visible)}
                >
                    <Icon name="arrow-right" size="24" />
                    <Icon name="close" size="24" />
                </button>
                <div className={styles.foot}>
                    <Theme className={styles.theme} visibleSidebar={visible} />
                </div>
            </div>
            <Help
                visible={visibleHelp}
                setVisible={setVisibleHelp}
                onClose={onClose}
            />
            <div
                className={cn(styles.overlay, { [styles.active]: visible })}
                onClick={() => setVisible(false)}
            ></div>
        </>
    );
};

export default Sidebar;
