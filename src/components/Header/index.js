import React, { useState } from "react";
import styles from "./Header.module.sass";
import Notification from "./Notification";
import User from "./User";
import { Link } from "react-router-dom";

const Header = ({ onOpen }) => {
  const [visible, setVisible] = useState(false);
  const handleClick = () => {
    onOpen();
    setVisible(false);
  };
  if(visible){}
  return (
    <header className={styles.header}>
      <button className={styles.burger} onClick={() => handleClick()}></button>
      
      
      <div className={styles.control} onClick={() => setVisible(false)}>
        <Link to="/calendar">
        <img src='images/calendar.png' alt="calendar" style={{ marginRight: "40px" }}/>
        </Link>
        
        <Notification className={styles.notification} />
        <User className={styles.user} />
      </div>
      {/* <div className={styles.btns}>
        <Link className={styles.link} to="/sign-in">
          Sign in
        </Link>
        <Link className={cn("button", styles.button)} to="/sign-up">
          Sign up
        </Link>
      </div> */}
    </header>
  );
};

export default Header;
